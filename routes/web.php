<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Frontend\HomeController@loadHomePage');
Route::get('/{store_name}', 'Frontend\StoreController@loadStorePage');
Route::get('/category/{category_name}', 'Frontend\CategoryController@loadCategoryPage');

Route::get('/coupon-details/{id}', 'Frontend\OfferController@loadOfferPage');
//Route::resource('store', 'StoreController');
Route::post('add_offer_working','Frontend\OfferController@addOfferWorking');

Route::post('add_subscriber','Frontend\HomeController@add_subscriber');
Route::get('/stores/list','Frontend\StoreController@get_all_stores');