<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\frontend\Categories;
use App\Models\frontend\Offers;
use App\Models\frontend\Store;

class CategoryController extends Controller
{
    public function __construct()
    {
    	$this->category = new Categories();
    	$this->offers = new Offers();
        $this->store = new Store();
    }

   public function loadCategoryPage(Request $request,$category)
    {
        $get_category_details = $this->category->getCategoryDetails($category);
        if(!isset($get_category_details->id))
        {
            dd("404");
        }

        $get_category_offers = $this->offers->getCategoryOffers($get_category_details->id);
        $get_category_offers_count = $this->offers->getCategoryOffersCount($get_category_details->id);
        $coupon_count = 0;
        $deal_count = 0;

        if(!empty($get_store_offers_count)){
            $coupon_count = $get_store_offers_count[0]->coupon_count;
            $deal_count = $get_store_offers_count[0]->deal_count;
        }

        $popular_stores = $this->store->getPopularStores();

        $popular_offers = $this->offers->getPopularOffers();
        
        return view ('frontend.category_page',compact('get_category_offers','get_category_details','coupon_count','deal_count','popular_stores','popular_offers'));
    }
}
