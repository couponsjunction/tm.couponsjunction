<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\frontend\Store;
use App\Models\frontend\Offers;
use MetaTag;

class StoreController extends Controller
{
    public function __construct()
    {
    	$this->store = new Store();
    	$this->offers = new Offers();
    }

   public function loadStorePage(Request $request,$store)
    {
        $get_store_details = $this->store->getStoreDetails($store);
        if(!isset($get_store_details->id))
        {
            dd("404");
        }

        MetaTag::set('title', $get_store_details->store_meta_title);
        MetaTag::set('description', $get_store_details->store_meta_des);

        $get_store_offers = $this->offers->getStoreOffers($get_store_details->id);
        $get_store_offers_count = $this->offers->getStoreOffersCount($get_store_details->id);
        $coupon_count = 0;
        $deal_count = 0;
        if(!empty($get_store_offers_count)){
            $coupon_count = $get_store_offers_count[0]->coupon_count;
            $deal_count = $get_store_offers_count[0]->deal_count;
        }

        $popular_stores = $this->store->getPopularStores();

        $popular_offers = $this->offers->getPopularOffers();
        return view ('frontend.store_page',compact('get_store_offers','get_store_details','coupon_count','deal_count','popular_stores','popular_offers'));
    }

    public function get_all_stores(Request $request)
    {
        $get_all_stores = $this->store->getAllStores();

        MetaTag::set('title', 'All Stores Coupons, Deals & Promo Codes on CouponsJunction');
        MetaTag::set('description', 'CouponsJunction is the all in one destination for the latest coupons and best offers from stores like Myntra, Flipkart, Uber, Swiggy & More.');

        return view('frontend.stores_list',compact('get_all_stores'));
    }
}
