<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\frontend\Store;
use App\Models\frontend\Offers;
use App\Models\frontend\Banners;
use App\Models\frontend\Categories;

class HomeController extends Controller
{

   public function __construct()
    {	
    	$this->store = new Store();
    	$this->offers = new Offers();
    	$this->banners = new Banners();
    	$this->categories = new Categories();
    }

    //changes for pull request
    public function loadHomePage(Request $request)
    {   
    	$popular_stores = $this->store->getPopularStores();
    	$popular_offers = $this->offers->getPopularOffers();
    	$get_banners = $this->banners->getBanners();
    	$get_top_categories = $this->categories->getTopCategories();

    	$get_top_categories_offers = $this->offers->topCategoryOffers(array_column($get_top_categories, 'id'));
    	$get_top_categories_offers_array = [];

    	if(!empty($get_top_categories_offers))
    	{
    		foreach($get_top_categories_offers as $key=>$category_offer)
    		{
                    if(isset($get_top_categories_offers_array[$category_offer->category_id]) && count($get_top_categories_offers_array[$category_offer->category_id]) > 3)
                    {
                        continue;
                    }
                    else
                    {
                        $get_top_categories_offers_array[$category_offer->category_id][] = $category_offer;    
                    }
    				
    			
    		}
    	}

        $test = "";
    	return view('welcome',compact('popular_stores','popular_offers','get_banners','get_top_categories_offers','get_top_categories_offers_array','get_top_categories'));
    }

    public function add_subscriber(Request $request){
        $result = [
            "token" => 0,
            "msg" => "You are already subscribed"
        ];
        // echo "<pre>";print_r($request->email);die;
        if($request->input('email')) {
            $email_avail = \DB::table('coupon_subscibers')->where(['email'=>$request->email])->first();
            // echo "<pre>";print_r($email_avail);die;
            if(empty($email_avail)){
                $data = [
                    "email" => $request->email
                ];
                \DB::table('coupon_subscibers')->insert($data);
                $result = [
                    "token" => 1,
                    "msg" => "You are successfully subscribed !!"
                ];
            }
        }
        return $result;
    }
}
