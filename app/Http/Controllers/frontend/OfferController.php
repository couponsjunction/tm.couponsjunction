<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\frontend\Offers;
use MetaTag;

class OfferController extends Controller
{
	public function __construct()
    {
    	$this->offers = new Offers();
    }

    public function loadOfferPage(Request $request,$offer){
    	$offer_details = $this->offers->getStoreOfferDetails($offer);
    	$offer_details = $offer_details[0];
    	$related_offer = $this->offers->getRelatedOffers($offer,$offer_details->store_id);
        MetaTag::set('title', $offer_details->offer_name);
        MetaTag::set('description', $offer_details->offer_name);

    	// echo "<pre>";print_r($offer_details->offer_type);die;
    	return view ('frontend.coupon_details',compact('offer_details','related_offer'));
    }

    public function addOfferWorking(Request $request)
    {

        $offer_id = $request->offer;
        $type = $request->type;

        $add_offer_working = $this->offers->addOfferWorkingInModal($offer_id,$type);

        $positive_count = $add_offer_working->offer_working_count;
        $negative_count = $add_offer_working->offer_not_working_count;

        $x = $positive_count;
        $total = $positive_count+$negative_count;
        $percentage = ($x*100)/$total;

        return [$positive_count,$negative_count,round(number_format($percentage, 2))];

    }
}
