<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Store;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $store = Store::where('store_name', 'LIKE', "%$keyword%")
                ->orWhere('store_description', 'LIKE', "%$keyword%")
                ->orWhere('store_url', 'LIKE', "%$keyword%")
                ->orWhere('store_slug', 'LIKE', "%$keyword%")
                ->orWhere('store_logo', 'LIKE', "%$keyword%")
                ->orWhere('store_meta_title', 'LIKE', "%$keyword%")
                ->orWhere('store_meta_des', 'LIKE', "%$keyword%")
                ->orWhere('is_popular_store', 'LIKE', "%$keyword%")
                ->orWhere('store_order', 'LIKE', "%$keyword%")
                ->orWhere('store_affiliate_url', 'LIKE', "%$keyword%")
                ->orWhere('is_campaign_active', 'LIKE', "%$keyword%")
                ->orWhere('is_deleted', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $store = Store::latest()->paginate($perPage);
        }

        return view('store.index', compact('store'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('store.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Store::create($requestData);

        return redirect('store')->with('flash_message', 'Store added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $store = Store::findOrFail($id);

        return view('store.show', compact('store'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $store = Store::findOrFail($id);

        return view('store.edit', compact('store'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $store = Store::findOrFail($id);
        $store->update($requestData);

        return redirect('store')->with('flash_message', 'Store updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Store::destroy($id);

        return redirect('store')->with('flash_message', 'Store deleted!');
    }
}
