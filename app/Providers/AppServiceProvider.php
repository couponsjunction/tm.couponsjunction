<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\frontend\Store;
use App\Models\frontend\Categories;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $stores = Store::getStores();
        view()->share('stores', $stores);

        $categories = Categories::getCategories();
        view()->share('categories', $categories);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
