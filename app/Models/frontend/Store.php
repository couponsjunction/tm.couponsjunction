<?php

namespace App\Models\frontend;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $table = "coupon_stores";

    public function getPopularStores()
    {
    	$popularStores = Store::where('is_popular_store',1)
    					 ->where('is_deleted',0)
    					 ->get();

        return $popularStores;
    }

    public function getStoreDetails($store_slug)
    {
    	$store_details = Store::where('store_slug',$store_slug)
    					->first();
    	return $store_details;
    }

    public static function getStores()
    {
        $stores = Store::where('is_deleted',0)
                ->limit(5)
                ->select('id','store_name','store_slug','store_logo')
                ->orderBy('store_order')
                ->get();
        return $stores;
    }

    public function getAllStores()
    {
        $all_stores = Store::where('is_deleted',0)
                     ->orderBy('store_name')
                     ->get();

        return $all_stores;

    }
    
}
