<?php
namespace App\Models\frontend;

use Illuminate\Database\Eloquent\Model;

class Banners extends Model
{
    protected $table = "coupon_banners";

    public function getBanners()
    {
    	$banners = Banners::where('banner_end_date','>=',date('Y-m-d H:i:s'))
                         ->where('banner_start_date','<=',date('Y-m-d H:i:s'))
                         ->where('is_deleted',0)
    					 ->orderBy('banner_order')
    					 ->get();

        return $banners;
    }

    
}
