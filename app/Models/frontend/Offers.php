<?php

namespace App\Models\frontend;

use Illuminate\Database\Eloquent\Model;

class Offers extends Model
{
    protected $table = "coupon_offers";

    public function getStoreOffers($store_id)
    {
    	$get_store_offers = Offers::where('store_id',$store_id)
    						->where('offer_end_date','>=',date('Y-m-d H:i:s'))
    						// ->where('offer_start_date','<=',date('Y-m-d H:i:s'))
    						->orderBy('offer_order')
    						->get();
    	return $get_store_offers;
    }

    public function getCategoryOffers($category_id)
    {
        $get_category_offers = Offers::where('category_id',$category_id)
                            ->join('coupon_stores','store_id','=','coupon_stores.id')
                            ->where('offer_end_date','>=',date('Y-m-d H:i:s'))
                            // ->where('offer_start_date','<=',date('Y-m-d H:i:s'))
                            ->orderBy('offer_order')
                            ->select('coupon_offers.*','coupon_stores.store_name','coupon_stores.store_logo')
                            ->get();

        return $get_category_offers;
    }


    public function getStoreOffersCount($store_id)
    {
        $get_store_offers_count = Offers::where('store_id',$store_id)
                            ->select(\DB::raw('COUNT(CASE offer_type WHEN "1" THEN 1 ELSE NULL END) AS coupon_count'),\DB::raw('COUNT(CASE offer_type WHEN "2" THEN 1 ELSE NULL END) AS deal_count'))
                            ->where('offer_end_date','>=',date('Y-m-d H:i:s'))
                            // ->where('offer_start_date','<=',date('Y-m-d H:i:s'))
                            ->orderBy('offer_order')
                            ->get();
        return $get_store_offers_count;
    }



    public function getCategoryOffersCount($category_id)
    {
        $get_category_offers_count = Offers::where('category_id',$category_id)
                            ->select(\DB::raw('COUNT(CASE offer_type WHEN "1" THEN 1 ELSE NULL END) AS coupon_count'),\DB::raw('COUNT(CASE offer_type WHEN "2" THEN 1 ELSE NULL END) AS deal_count'))
                            ->where('offer_end_date','>=',date('Y-m-d H:i:s'))
                            // ->where('offer_start_date','<=',date('Y-m-d H:i:s'))
                            ->orderBy('offer_order')
                            ->get();
        return $get_category_offers_count;
    }


    public function getPopularOffers()
    {
        $get_popular_offers = Offers::join('coupon_stores','store_id','=','coupon_stores.id')
                                      ->where('coupon_offers.is_popular',1)
                                      ->where('offer_end_date','>=',date('Y-m-d H:i:s'))
                                      ->select('coupon_offers.*','coupon_stores.store_logo')
                                      ->limit(4)
                                      ->get();

        return $get_popular_offers;
    }

    public function topCategoryOffers($categoryIds)
    {
        $top_category_offers = Offers::join('coupon_stores','store_id','=','coupon_stores.id')
                                      ->whereIn('category_id',$categoryIds)
                                      ->where('offer_end_date','>=',date('Y-m-d H:i:s'))
                                      ->orderBy('offer_order')
                                      ->select('coupon_offers.*','coupon_stores.store_logo')
                                      ->get();

        return $top_category_offers;
    }


    public function getStoreOfferDetails($offer_id)
    {
        $get_offer_details = Offers::join('coupon_stores','store_id','=','coupon_stores.id')
                            ->select('coupon_offers.*','coupon_stores.store_logo')
                            ->where('coupon_offers.id',$offer_id)
                            ->get();
        // echo "<pre>";print_r($get_offer_details);die;
        return $get_offer_details;
    }

    public function getRelatedOffers($offer_id,$store_id)
    {
        $get_related_offers = Offers::join('coupon_stores','store_id','=','coupon_stores.id')
                            ->where('coupon_offers.id','!=',$offer_id)
                            ->where('coupon_offers.store_id','=',$store_id)
                            ->limit(4)
                            ->get();
        // echo "<pre>";print_r($get_offer_details);die;
        return $get_related_offers;
    }

    public function addOfferWorkingInModal($offer_id,$type)
    {
        $update_offer_working = Offers::where('id',$offer_id)
                                ->increment($type);

        $select_new_working = Offers::where('id',$offer_id)
                              ->select('offer_working_count','offer_not_working_count')
                              ->first();

        return $select_new_working;


    }
}
