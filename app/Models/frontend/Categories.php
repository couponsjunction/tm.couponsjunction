<?php

namespace App\Models\frontend;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = "coupon_categories";

    public function getTopCategories()
    {
    	$popularCategories = Categories::where('is_deleted',0)
                         ->orderBy('category_order')
                         ->limit(4)
                         ->select('category_name','id','category_slug')
                         ->get()
                         ->toArray();
    					 

        return $popularCategories;
    }

    public function getCategoryDetails($category_slug)
    {
    	$category_details = Categories::where('category_slug',$category_slug)
    					->first();
    	return $category_details;
    }

    public static function getCategories()
    {
        $categories = Categories::where('is_deleted',0)
                ->limit(5)
                ->select('id','category_name','category_slug')
                ->orderBy('category_order')
                ->get();
        return $categories;
    }
    
}
