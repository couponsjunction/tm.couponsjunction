<?php

namespace App\Models\frontend;

use Illuminate\Database\Eloquent\Model;

class CouponType extends Model
{
    protected $table = "coupon_type";
    public $timestamps = false;


    public function scopePopular($query)
    {
        return $query->where('is_popular_offer', '=', 1);
    }

}
