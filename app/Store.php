<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'coupon_stores';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['store_name', 'store_description', 'store_url', 'store_slug', 'store_logo', 'store_meta_title', 'store_meta_des', 'is_popular_store', 'store_order', 'store_affiliate_url', 'is_campaign_active', 'is_deleted'];

    
}
