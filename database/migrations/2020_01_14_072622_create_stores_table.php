<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_stores', function (Blueprint $table) {
            $table->increments('id');  
            $table->string('store_name', 100)->comment('Store Name'); 
            $table->text('store_description')->comment('Store Description'); 
            $table->string('store_logo', 25)->comment('Store Logo');
            $table->string('store_meta_title', 100)->comment('Store Meta Title');
            $table->string('store_meta_description', 200)->comment('Store Meta Description');
            $table->integer('is_popular_store')->length(2)->unsigned()->default(0)->comment('Is Popular Store?');
            $table->integer('store_order')->length(2)->unsigned()->comment('Order to Display in list');
            $table->string('store_url', 100)->comment('Store URL');       
            $table->string('store_affiliate_url', 100)->comment('Store Affiliate URL');       
            $table->integer('is_campaign_active')->length(2)->unsigned()->default(0)->comment('Is Store Campaign Active');
            $table->integer('is_deleted')->length(2)->unsigned()->default(0)->comment('Soft Delete Field');       
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_stores');
    }
}
