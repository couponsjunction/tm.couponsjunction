<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type_name',20)->comment('Coupon Type Name');
            $table->integer('is_deleted')->length(2)->unsigned()->default(0)->comment('Field for Soft Delete');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_type');
    }
}
