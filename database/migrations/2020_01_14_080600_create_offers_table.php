<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_offers', function (Blueprint $table) {
            $table->increments('id')->comment('Primary Id');
            $table->string('offer_name', 255)->comment('Offer Name'); 
            $table->string('offer_slug', 255)->comment('Offer Slug'); 
            $table->text('offer_description')->comment('Offer Description'); 
            $table->integer('store_id')->length(11)->unsigned()->comment('Store Id');
            $table->foreign('store_id')->references('id')->on('coupon_stores');
            $table->integer('offer_type')->length(11)->unsigned()->comment('Offer Type Id');
            $table->foreign('offer_type')->references('id')->on('coupon_type');
            $table->dateTime('offer_start_date')->comment('Offer Start Date');
            $table->dateTime('offer_end_date')->comment('Offer End Date');
            $table->integer('offer_order')->length(2)->unsigned()->comment('Order to Display in list'); 
            $table->integer('is_popular_offer')->length(2)->unsigned()->comment('Is Popular Offer?');
            $table->string('offer_image', 100)->comment('Offer Image if available')->nullable();
            $table->integer('is_dealoftheday')->length(2)->unsigned()->default(0)->comment('Is Deal of the Day?');
            $table->integer('is_deleted')->length(2)->unsigned()->default(0)->comment('Soft Delete Field');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_offers');
    }
}
