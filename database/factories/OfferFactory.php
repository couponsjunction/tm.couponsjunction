<?php

use Faker\Generator as Faker;

$factory->define(App\Models\frontend\Offers::class, function (Faker $faker) {
    return [
        'offer_name' => $faker->sentence(4),
        'offer_description' => $faker->sentence(20),
        'store_id'=>1,
        'offer_start_date'=> $faker->dateTimeThisMonth(),
        'offer_end_date'=> $faker->dateTimeBetween('this week', '+30 days'),
        'offer_order'=>1,
     	'offer_slug'=> $faker->sentence(4),

    ];
});
