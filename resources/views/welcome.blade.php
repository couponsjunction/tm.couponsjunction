@include('frontend.layouts.header')
@include('frontend.layouts.head')

<div class="top-area">

            <div id="owl-demo" class="owl-carousel owl-theme">
                 @if(!$get_banners->isEmpty())
                    @foreach($get_banners as $key=>$banner)
                        <div class="item"><a href="{{$banner->banner_url}}" target="_blank" rel="nofollow"><img src="{{asset('storage/image/banners/'.$banner->banner_image)}}" alt="{{$banner->banner_name}}"/></a></div>
                    @endforeach
                @endif
                  
                  
            </div>

            <!-- <div class="mod-head-slide">
                <div class="grid_frame">
                    <div class="wrap-slide">
                        <p class="ta-c"><img src="images/ajax-loader.gif" alt="loading"></p>
                        <div id="sys_head_slide" class="head-slide flexslider">
                            <ul class="slides">
                                <li>
                                    <img src="{{asset('public/images/ex/01_banner.jpg')}}" alt=""/>
                                </li>
                                <li>
                                    <img src="{{asset('public/images/ex/02_banner.jpg')}}" alt=""/>
                                </li>
                                <li>
                                    <img src="{{asset('public/images/ex/03_banner.jpg')}}" alt=""/>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>

        
        <div class="grid_frame page-content">
            <div class="container_grid">
                 <div class="mod-brands block clearfix home_page_brands">
                    <div class="grid_12">
                        <h3 class="title-block has-link">
                            POPULAR STORES
                            <a href="{{url('stores/list')}}" class="link-right">View All Stores<i class="pick-right"></i></a>
                        </h3>
                    </div>
                    <div class="block-content list-brand clearfix popular_store_box">

                        @if(!$popular_stores->isEmpty())
                            @foreach($popular_stores as $key=>$store)
                                <div class="brand-item grid_2">
                                        <div class="brand-content">
                                            <div class="brand-logo">
                                                <div class="wrap-img-logo">
                                                    <span class="ver_hold"></span>
                                                    <a href="{{url('/').'/'.$store->store_slug}}" class="ver_container"><img src="{{$store->store_logo}}" alt="{{$store->store_name}} Coupons & Offers"></a>
                                                </div>
                                            </div>
                                        </div>
                                </div><!--end: .brand-item -->
                            @endforeach
                        @endif
                        
                        </div><!--end: .brand-item -->
                    </div>
                </div><!--end: .mod-brand -->


                <div class="mod-grp-coupon block clearfix">
                    <div class="grid_12">
                        <h3 class="title-block has-link">
                            HOTTEST OFFERS OF THE DAY
                            <!-- <a href="#" class="link-right">See all <i class="pick-right"></i></a> -->
                        </h3>
                    </div>
                    <div class="block-content list-coupon clearfix">
                         @if(!$popular_offers->isEmpty())
                            @foreach($popular_offers as $key=>$offer)
                        <div class="coupon-item grid_3">
                            <div class="coupon-content popular_offers_content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="{{$offer->store_logo}}" alt="$COUPON_TITLE"></a>
                                    </div>
                                </div>
                                <div class="coupon-price">{{$offer->offer_name}}</div>
                                <div class="coupon-brand"></div>
                                <!-- <div class="coupon-desc">Find Parts for All Major Brands at Sears PartsDirect </div> -->
                               @if($offer->offer_type == 1)
                                <a class="btn btn-blue btn-take-coupon btn-view-coupon" href="{{$offer->offer_url}}" data-url="{{url('/coupon-details').'/'.$offer->id}}" target="_blank" rel="nofollow">Get Coupon</a>
                               @else
                               <a class="btn btn-blue btn-take-coupon btn-view-coupon" href="{{$offer->offer_url}}" data-url="{{url('/coupon-details').'/'.$offer->id}}" target="_blank" rel="nofollow">Get Deal</a>
                               @endif
                                
                            </div>
                            <!-- <i class="stick-lbl hot-sale"></i> -->
                        </div><!--end: .coupon-item -->
                            @endforeach
                        @endif

                    </div>
                </div><!--end block: New Coupons-->

                @if(!empty($get_top_categories))
                    @foreach($get_top_categories as $key=>$category)
                        <div class="mod-grp-coupon block clearfix">
                    <div class="grid_12">
                        <h3 class="title-block has-link">
                            {{$category['category_name']}} Offers
                            <a href="{{url('/').'/category/'.$category['category_slug']}}" class="link-right">View all <i class="pick-right"></i></a>
                        </h3>
                    </div>
                    
                    @if(isset($get_top_categories_offers_array[$category['id']]))
                        <div class="block-content list-coupon clearfix">
                        @foreach($get_top_categories_offers_array[$category['id']] as $key1=>$offer)
                             <div class="coupon-item grid_3">
                            <div class="coupon-content popular_offers_content">
                                <div class="img-thumb-center">
                                    <div class="wrap-img-thumb">
                                        <span class="ver_hold"></span>
                                        <a href="#" class="ver_container"><img src="{{$offer->store_logo}}" alt="$COUPON_TITLE"></a>
                                    </div>
                                </div>
                                <div class="coupon-price">{{$offer->offer_name}}</div>
                                <!-- <div class="coupon-brand">Wallmart</div> -->
                                <!-- <div class="coupon-desc">Find Parts for All Major Brands at Sears PartsDirect </div> -->
                                <!-- <div class="time-left">9 days 4 hours left</div> -->
                              @if($offer->offer_type == 1)
                                <a class="btn btn-blue btn-take-coupon btn-view-coupon" href="{{$offer->offer_url}}" data-url="{{url('/coupon-details').'/'.$offer->id}}" target="_blank" rel="nofollow">Get Coupon</a>
                               @else
                               <a class="btn btn-blue btn-take-coupon btn-view-coupon" href="{{$offer->offer_url}}" data-url="{{url('/coupon-details').'/'.$offer->id}}" target="_blank" rel="nofollow">Get Deal</a>
                               @endif
                            </div>
                            <!-- <i class="stick-lbl hot-sale"></i> -->
                        </div><!--end: .coupon-item -->
                        @endforeach
                    @endif
                    
                    </div>
                    <!-- <a class="grid_6 btn btn-orange btn-load-more" href="#">Load more coupon</a> -->
                </div><!--end block: Featured Coupons-->
                    @endforeach
                @endif
                
                <div class="mod-email-newsletter clearfix">
                    <div class="grid_12">
                        <div class="wrap-form clearfix">
                            <div class="left-lbl">
                                <div class="big-lbl">newsletter</div>
                                <div class="sml-lbl">Don't miss a chance!</div>
                            </div>
                            <form action="#" method="post" name="coupon_subscriber" id="add_coupon_subscriber">
                            <div class="wrap-email">
                                <label for="sys_email_newsletter">
                                    <input type="email" id="sys_email_newsletter" name="email" required="" placeholder="Enter your email here"/>
                                </label>
                                <p class="subscriber_msg"></p>
                            </div>
                            <button class="btn btn-orange btn-submit-email" type="submit">SUBSCRIBE NOW</button>
                            </form>
                        </div>
                    </div>
                </div><!--end: .mod-email-newsletter-->
                
            </div>
        </div>

@include('frontend.layouts.footer')