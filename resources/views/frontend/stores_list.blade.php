@include('frontend.layouts.header')
@include('frontend.layouts.head')
 
  <!-- <div class="top-area">
            <div class="grid_frame">
                <div class="container_grid clearfix">
                    <div class="grid_12">
                        <h2 class="page-title">Coupon Code</h2>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="grid_frame page-content">
            <div class="container_grid">
                  <div class="mod-brand-detail-4 clearfix">
                    <div class="grid_12">
                        <div class="brand-top-info clearfix">
                            
                            <div class="brand-desc category-brand-desc">
                                <div class="title-desc">Coupons Junction Stores List</div>
                                <p class="rs">We have covered various categories stores be it Food ordering App Like Swiggy, Shopping website like Flipkart, Grocery stores like Bigbasket, Cab service like Uber, gifting website like Ferns N Petal and more. Find the list of all Stores for which you can get offers.</p>
                            </div>
                            
                        </div>
                    </div>
                </div>

                 
                <div class="layout-2cols clearfix">
                    <div class="grid_12 content">

                          <div class="store_az_div">
                             <ul class="store_az_ul">
                                <li class="store_az_li active">All</li>
                                @foreach (range('A', 'Z') as $column)
                                    <li class="store_az_li">{{$column}}</li>
                                @endforeach
                            </ul>
                        </div>
                            <div class="mod-list-store block">
                            <div class="block-content">
                            <div class="wrap-list-store clearfix">
                               @if(!$get_all_stores->isEmpty())

                                 @foreach($get_all_stores as $key=>$store)
                                     <a class="brand-logo store_list_logo" href="{{url('/').'/'.$store->store_slug}}" data-filter="{{substr($store->store_name, 0, 1)}}">
                                            <span class="wrap-logo">
                                                <span class="center-img">
                                                    <span class="ver_hold"></span>
                                                    <span class="ver_container"><img src="{{$store->store_logo}}" alt="{{$store->store_name}}"></span>
                                                </span>
                                            </span>
                                        </a>

                                 @endforeach
                               @endif
                            </div>
                            </div>
                        </div>
                            
                            
                           
                    </div>
                    <div class="grid_4 sidebar">
                        <!-- <div class="mod-search block">
                            <h3 class="title-block">Find your coupon code</h3>
                            <div class="block-content">
                                <label class="lbl-wrap" for="sys_search_coupon_code">
                                    <input class="keyword-search" id="sys_search_coupon_code" type="search" placeholder="Search"/>
                                    <input type="submit" class="btn-search" value="">
                                </label>
                            </div>
                        </div> --><!--end: .mod-search -->
                       
                       
                        <!-- <div class="mod-ads"><a href="#"><img src="images/ex/04-17.jpg" alt="$NAME"/></a></div> -->
                        <!-- <div class="mod-popular-tag block">
                            <h3 class="title-block">Popular Tag</h3>
                            <div class="block-content">
                                <a class="btn btn-gray type-tag" href="#">Sweet</a>
                                <a class="btn btn-gray type-tag" href="#">Lindor</a>
                                <a class="btn btn-gray type-tag" href="#">Food</a>
                                <a class="btn btn-gray type-tag" href="#">Lindt</a>
                                <a class="btn btn-gray type-tag" href="#">Walmart</a>
                                <a class="btn btn-gray type-tag" href="#">Chocolate</a>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>

@include('frontend.layouts.footer')