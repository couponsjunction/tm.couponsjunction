@include('frontend.layouts.header')
@include('frontend.layouts.head')
 
  <!-- <div class="top-area">
            <div class="grid_frame">
                <div class="container_grid clearfix">
                    <div class="grid_12">
                        <h2 class="page-title">Coupon Code</h2>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="grid_frame page-content">
            <div class="container_grid">
                  <div class="mod-brand-detail-4 clearfix">
                    <div class="grid_12">
                        <div class="brand-top-info clearfix">
                            
                            <div class="brand-desc category-brand-desc">
                                <div class="title-desc">{{$get_category_details->category_name}} Coupons and Offers</div>
                                <p class="rs">{{$get_category_details->category_description}}</p>
                            </div>
                            <div class="right-counter">
                                <div class="wrap-content">
                                    
                                    <div class="count-info clearfix">
                                        <span class="lbl">Coupons</span>
                                        <span class="val">{{$coupon_count}}</span>
                                    </div>
                                    <div class="count-info clearfix">
                                        <span class="lbl">Deals</span>
                                        <span class="val">{{$deal_count}}</span>
                                    </div>
                                    
                                    <!-- <a class="btn btn-blue btn-follow-brand" href="#">Follow brand</a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                 <div id="sys_mod_filter" class="mod-filter">
            <div class="grid_frame">
                <div class="container_grid clearfix">
                    <div class="grid_12">
                        <div class="lbl-search">
                            <input class="txt-search" id="sys_txt_search" type="search" placeholder="Search"/>
                            <input type="submit" class="btn-search" value=""/>
                        </div>
                        <div class="select-cate">
                            <!-- <div id="sys_selected_val" class="show-val">
                                <span data-cate-id="0">All type</span>
                                <i class="pick-down"></i>
                            </div> -->
                            <!-- <div id="sys_list_dd_cate" class="dropdown-cate">
                                <div class="first-lbl">All Categories</div>
                                <div class="wrap-list-cate clearfix">
                                    <a href="#" data-cate-id="1">Baby & Toddler</a>
                                    <a href="#" data-cate-id="2">Automotive </a>
                                    <a href="#" data-cate-id="3">Beverages</a>
                                    <a href="#" data-cate-id="4">Books & Magazines</a>
                                    <a href="#" data-cate-id="5">Foods </a>
                                    <a href="#" data-cate-id="6">Health Care</a>
                                    <a href="#" data-cate-id="7">Home Entertainment</a>
                                    <a href="#" data-cate-id="8">Personal Care </a>
                                    <a href="#" data-cate-id="9">Pet Care </a>
                                    <a href="#" data-cate-id="10">Professional Services </a>
                                    <a href="#" data-cate-id="11">Toys and Games</a>
                                    <a href="#" data-cate-id="12">Coupon Codes</a>
                                    <a href="#" data-cate-id="13">Recipes</a>
                                    <a href="#" data-cate-id="14">Household </a>
                                </div>
                            </div> -->
                        </div><!--end: .select-cate -->
                        <!-- <div class="range-days-left">
                            <span class="lbl-day">Days left</span>
                            <span id="sys_min_day" class="min-day"></span>
                            <div id="sys_filter_days_left" class="filter-days"></div>
                            <span id="sys_max_day" class="max-day"></span>
                        </div> --><!--end: .range-days-left -->
                        <!-- <input id="sys_apply_filter" class="btn btn-red type-1 btn-apply-filter" type="button" value="Apply Filter"> -->
                    </div>
                </div>

            </div>
        </div><!--end: .mod-filter -->
                <div class="layout-2cols clearfix">
                    <div class="grid_8 content">
                        <div class="mod-coupons-code">
                            <div class="wrap-list">
                                @if(!$get_category_offers->isEmpty())
                                    @foreach($get_category_offers as $category_offer)
                                        <div class="coupons-code-item right-action flex">
                                        
                                        <div class="right-content flex-body category-content">
                                            <div class="brand-logo thumb-left">
                                                <div class="wrap-logo">
                                                    <div class="center-img">
                                                        <span class="ver_hold"></span>
                                                        <a href="#" class="ver_container"><img src="{{$category_offer->store_logo}}" alt="{{$category_offer->store_name}}"></a>
                                                    </div>
                                                </div>
                                            </div>

                                            <h3 class="rs save-price"><a href="#">{{$category_offer->offer_name}}</a></h3>
                                            <span class="show_description">Show Description</span>

                                            <div class="rs coupon-desc">{!!$category_offer->offer_description!!}</div>
                                            <div class="bottom-action">
                                                <div class="left-vote">
                                                    <span class="lbl-work">100% working</span>
                                                    <span>
                                                        <span class="lbl-vote add-vote" id="vote_{{$category_offer->id}}" data-offer="{{$category_offer->id}}" data-type="offer_working_count">{{$category_offer->offer_working_count}} <i class="icon iAddVote"></i></span>
                                                        <span class="lbl-vote add-vote" id="negativevote_{{$category_offer->id}}" data-offer="{{$category_offer->id}}" data-type="offer_not_working_count">{{$category_offer->offer_not_working_count}} <i class="icon iSubVote"></i></span>
                                                    </span>
                                                </div>
                                                @if($category_offer->offer_type == 1)
                                                <a class="btn btn-blue btn-view-coupon" href="{{$category_offer->offer_url}}" data-url="{{url('/coupon-details').'/'.$category_offer->id}}" target="_blank" rel="nofollow">VIEW <span>COUPON</span> CODE</a>
                                                @else
                                                <a class="btn btn-blue btn-view-coupon" href="{{$category_offer->offer_url}}" data-url="{{url('/coupon-details').'/'.$category_offer->id}}" target="_blank" rel="nofollow">Get <span>COUPON</span> DEAL</a>
                                                @endif
                                            </div>
                                        </div>
                                        </div><!--end: .coupons-code-item -->
                                    @endforeach
                                @endif
                            </div>
                            <!-- <div class="pagination">
                                <a class="page-nav" href="#"><i class="icon iPrev"></i></a>
                                <a class="page-num active" href="#">1</a>
                                <a class="page-num" href="#">2</a>
                                <a class="page-num" href="#">3</a>
                                <a class="page-num" href="#">4</a>
                                <a class="page-num" href="#">5</a>
                                <a class="page-nav" href="#"><i class="icon iNext"></i></a>
                            </div> -->
                        </div><!--end: .mod-coupons-code -->
                    </div>
                    <div class="grid_4 sidebar">
                        <!-- <div class="mod-search block">
                            <h3 class="title-block">Find your coupon code</h3>
                            <div class="block-content">
                                <label class="lbl-wrap" for="sys_search_coupon_code">
                                    <input class="keyword-search" id="sys_search_coupon_code" type="search" placeholder="Search"/>
                                    <input type="submit" class="btn-search" value="">
                                </label>
                            </div>
                        </div> --><!--end: .mod-search -->
                        <div class="mod-list-store block">
                            <h3 class="title-block">Popular store</h3>
                            <div class="block-content">
                                <div class="wrap-list-store clearfix">
                                    @foreach($popular_stores as $key => $value)
                                        <a class="brand-logo" href="{{url('/').'/'.$value->store_slug}}" >
                                            <span class="wrap-logo">
                                                <span class="center-img">
                                                    <span class="ver_hold"></span>
                                                    <span class="ver_container"><img src="{{$value->store_logo}}" alt="{{$value->store_name}}"></span>
                                                </span>
                                            </span>
                                        </a>
                                    @endforeach                                   
                                </div>
                            </div>
                        </div><!--end: .mod-list-store -->
                        <div class="mod-simple-coupon block">
                            <h3 class="title-block">Latest coupon</h3>
                            <div class="block-content">
                                @foreach($popular_offers as $key => $value)
                                <div class="coupons-code-item simple flex">
                                    <div class="brand-logo thumb-left">
                                        <div class="wrap-logo">
                                            <div class="center-img">
                                                <span class="ver_hold"></span>
                                                <a href="#" class="ver_container"><img src="{{$value->store_logo}}" alt="{{$value->offer_slug}}"></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right-content flex-body">
                                        <p class="rs save-price"><a href="#">{{$value->offer_name}}</a></p>
                                    </div>
                                </div><!--end: .coupons-code-item -->
                                @endforeach
                            </div>
                        </div><!--end: .mod-simple-coupon -->
                        <!-- <div class="mod-ads"><a href="#"><img src="images/ex/04-17.jpg" alt="$NAME"/></a></div> -->
                        <!-- <div class="mod-popular-tag block">
                            <h3 class="title-block">Popular Tag</h3>
                            <div class="block-content">
                                <a class="btn btn-gray type-tag" href="#">Sweet</a>
                                <a class="btn btn-gray type-tag" href="#">Lindor</a>
                                <a class="btn btn-gray type-tag" href="#">Food</a>
                                <a class="btn btn-gray type-tag" href="#">Lindt</a>
                                <a class="btn btn-gray type-tag" href="#">Walmart</a>
                                <a class="btn btn-gray type-tag" href="#">Chocolate</a>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>

@include('frontend.layouts.footer')