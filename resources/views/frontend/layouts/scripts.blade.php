<script type="text/javascript" src="{{asset('public/js/jquery-1.10.2.js')}}"></script>
<script type="text/javascript" src="{{asset('public/js/jquery.flexslider-min.js')}}"></script>
<script type="text/javascript" src="{{asset('public/js/jquery.nouislider.js')}}"></script>
<script type="text/javascript" src="{{asset('public/js/jquery.popupcommon.js')}}"></script>
<script type="text/javascript" src="{{asset('public/js/html5lightbox.js')}}"></script>
<!--//js for responsive menu-->
<script type="text/javascript" src="{{asset('public/js/modernizr.custom.js')}}"></script>
<script type="text/javascript" src="{{asset('public/js/classie.js')}}"></script>
<script type="text/javascript" src="{{asset('public/js/mlpushmenu.js')}}"></script>

<script type="text/javascript" src="{{asset('public/js/script.js')}}"></script>

<script type="text/javascript" src="{{asset('public/js/owl.carousel.js')}}"></script>



<!--DO NOT ENTER ANY EXTERNAL LINK IN BETWEEN-->
<script type="text/javascript">

var offer_working_url = "{{URL::to('add_offer_working')}}";
var coupon_subscriber_url = "{{URL::to('add_subscriber')}}";

$(document).ready(function() {


	$('.owl-carousel').owlCarousel({
    loop:true,
    autoplay:true,
    margin:30,
    nav:false,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
        },
        600:{
            items:3,
        },
        1000:{
            items:1,
            nav:false,
            loop:true
        }
    }
});


$(".add-vote").click(function(e)
{
    if($(this).hasClass('vote-added'))
    {
        return true;
    }
     var offer_id = $(this).attr("data-offer");
     var type = $(this).attr('data-type');
     var element_id = $(this).attr('id');
     $(this).addClass('vote-added');

     if(type == "offer_working_count")
            {
                $(this).css('background-color','#A1C44E'); 
            }
            else
            {
                $(this).css('background-color','#D11F00'); 
            }

     $(this).parent().find('.lbl-vote-thanks').css('display','block !important');

     $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });

    $.ajax({

           type:'POST',

           url:offer_working_url,

           data:{offer:offer_id, type:type},

           success:function(data){

            if(type == "offer_working_count")
            {
                $('#'+element_id).html(data[0]+" <i class='fa fa-thumbs-o-up' aria-hidden='true'></i>");

            }
            else
            {
                $('#'+element_id).html(data[1]+" <i class='fa fa-thumbs-o-down' aria-hidden='true'></i>");
            }

            $('#'+element_id).parent().parent().find('.lbl-work').html(data[2]+'% working');

           }

        });
  
});


$(".btn-view-coupon").click(function(e)
{
     var page_url = $(this).attr('data-url');

     setTimeout(function(){location.href=page_url} , 1000);   

     return true;

});


$(".show_description").click(function(e)
{
    if ($(this).parent().find('.coupon-desc').is( ":hidden" ) ) {
        
        $(this).text('Hide Description');
        $(this).parent().find('.coupon-desc').slideDown( "slow" );
    }
    else
    {
        $(this).text('Show Description');
        $(this).parent().find('.coupon-desc').slideUp( "slow" );
    }

    
});

 $(".store-search").keyup(function() {
    
    var searchtext = $(this).val();
    var count = 0;
    var n = searchtext.length;
    if (n >= 3 || n == 0) {

      $('.coupons-code-item').each(function(i, obj) {

            var text = $(obj).find('.right-content').find('h3').text().toLowerCase();

            if(text.indexOf(searchtext) >= 0 )
            {
                count++;
                $(obj).show();
            }
            else {

                $(obj).hide();
            }

      });

      if(count == 0)
      {
         $(".search_no_offers").css('display','block');
      }
      else
      {
         $(".search_no_offers").css('display','none'); 
      }

}

});


$('#add_coupon_subscriber').submit(function(){

    var email = $('#sys_email_newsletter').val();

         $.ajaxSetup({

            headers: {

                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

            }

        });

    $.ajax({

       type:'POST',

       url:coupon_subscriber_url,

       data:{email:email},

       success:function(data){

        console.info(data);
        console.info(data.token);
        if(data.token == 1){
            $('.subscriber_msg').html(data.msg);
        }else{
            $('.subscriber_msg').html(data.msg);
        }
        
        return false;
       }

    });
    return false;
}); 

$(window).scroll(function(){
  var sticky = $('.mod-header'),
      scroll = $(window).scrollTop();

  if (scroll >= 100) sticky.addClass('fixed');
  else sticky.removeClass('fixed');
});

$(".store_az_li").click(function(e)
{
   var current_word = $(this).text();
    $(".store_az_li").removeClass('active'); 
    $(this).addClass('active');     
    $( ".store_list_logo" ).each(function( index ) {
       if($(this).attr('data-filter') == current_word || current_word == "All")
       {
         $(this).show(500);
       }
       else
       {
         $(this).hide(500);
       }
    });

});


});
</script>