<body class="grey"><!--<div class="alert_w_p_u"></div>-->
<div class="container-page">
    <div class="mp-pusher" id="mp-pusher">
        <header class="mod-header">
            <div class="grid_frame">
                <div class="container_grid clearfix">
                    <div class="grid_12">
                        <div class="header-content clearfix">
                            <h1 id="logo" class="rs">
                                <a href="{{url('/')}}">
                                    Coupons Junction
                                    <!-- <img src="{{asset('public/images/logo.png')}}" alt="Couponsjunction" /> -->
                                </a>
                            </h1>
                            <!-- <a id="sys_head_login" class="btn btn-green type-login btn-login" href="#">Login</a> -->
                            <?php //echo "<pre>";print_r($categories);die; ?>
                            <nav class="main-nav">
                                <ul id="main-menu" class="nav nav-horizontal clearfix">
                                    <li class="active">
                                        <a href="{{url('/')}}">Home</a>
                                    </li>
                                    <li class="has-sub">
                                        <a href="{{url('/')}}">Stores</a>
                                        @if($stores)
                                        <ul class="sub-menu">
                                            @foreach($stores as $store)
                                            <li><a href="{{url('/').'/'.$store['store_slug']}}">{{$store['store_name']}}</a></li>
                                            @endforeach
                                            <li><a href="{{url('/stores/list')}}">View All Stores</a></li>
                                        </ul>
                                        @endif
                                    </li>

                                    <li class="has-sub">
                                        <a href="brand-list.html">Categories</a>
                                        @if($categories)
                                        <ul class="sub-menu">
                                            @foreach($categories as $category)
                                            <li><a href="{{url('/').'/category/'.$category['category_slug']}}">{{$category['category_name']}}</a></li>
                                            @endforeach
                                        </ul>
                                        @endif
                                    </li>
                                    <li>
                                        <a href="{{url('/')}}">Coupons</a>
                                    </li>
                                    <!-- <li><a href="blog.html">Blog</a></li> -->
                                    <!-- <li>
                                        <a href="my-coupon.html">My coupons</a>
                                        <i class="icon iPickRed lbl-count"><span>12</span></i>
                                    </li> -->
                                </ul>
                                <a id="sys_btn_toogle_menu" class="btn-toogle-res-menu" href="#alternate-menu"></a>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sys_pop_login" class="pop-login">
                <div class="viewport-pop">
                    <div class="transport-viewer clearfix">
                        <div class="mod-register">
                            <h3 class="rs title-mod">Hello pretty! Welcome to Couponday.com</h3>
                            <div class="wrap-form-reg clearfix">
                                <form action="#">
                                    <div class="left-form">
                                        <label class="wrap-txt" for="sys_email">
                                            <input class="input-txt" id="sys_email" type="email" placeholder="you@mail.com">
                                        </label>
                                        <label class="wrap-txt" for="sys_pass">
                                            <input class="input-txt" id="sys_pass" type="password" placeholder="password please!">
                                        </label>
                                        <label class="wrap-check" for="sys_chk_news">
                                            <input id="sys_chk_news" class="input-chk" type="checkbox"> Remember me
                                            <i class="icon iUncheck"></i>
                                            <a class="lost-pass" href="#">Forgot password ?</a>
                                        </label>
                                        <div class="wrap-login-btn">
                                            <button class="btn-flat gr btn-submit-reg" type="submit">Login</button>
                                            <div class="sep-connect">
                                                <span>Or</span>
                                            </div>
                                            <div class="grp-connect">
                                                <a class="btn-flat fb" href="#">Facebook</a>
                                                <a class="btn-flat gg" href="#">Google</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right-create-acc">
                                        <img class="account" src="{{asset('public/images/reg-account.png')}}" alt="Couponday.com">
                                        <p class="lbl-dung-lo rs">Not a member? Don’t worry</p>
                                        <a id="sys_link_reg_panel" href="register.html" class="btn-flat yellow btn-submit-reg">Create an account</a>
                                        <div id="sys_warning_sms" class="warning-sms" data-warning-txt="No spam guarantee,No disturb,Promotion News"></div>
                                    </div>
                                </form>
                                <i class="line-sep"></i>
                            </div>
                        </div><!--end: Login panel -->
                        <div class="mod-register">
                            <h3 class="rs title-mod">Hello pretty! Welcome to Couponday.com</h3>
                            <div class="desc-reg">Sign up for free and get exclusive access to members-only savings, rewards and special promotions from Coupons.com. Enter in an email and a password or sign up with Facebook.</div>
                            <div class="wrap-form-reg clearfix">
                                <form action="#">
                                    <div class="left-form">
                                        <label class="wrap-txt" for="sys_email_reg">
                                            <input class="input-txt" id="sys_email_reg" type="email" placeholder="you@mail.com"/>
                                        </label>
                                        <label class="wrap-txt" for="sys_pass_reg">
                                            <input class="input-txt" id="sys_pass_reg" type="password" placeholder="password please!"/>
                                        </label>
                                        <label class="wrap-check" for="sys_chk_news_reg">
                                            <input id="sys_chk_news_reg" class="input-chk" type="checkbox"/> Send me the weekly Couponday.com’s offers.
                                            <i class="icon iUncheck"></i>
                                        </label>
                                        <label class="wrap-check" for="sys_chk_agree">
                                            <input id="sys_chk_agree" class="input-chk" type="checkbox"/> I agree to the <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>.
                                            <i class="icon iUncheck"></i>
                                        </label>
                                    </div>
                                    <div class="right-connect">
                                        <button class="btn-flat yellow btn-submit-reg" type="submit">Create an account</button>
                                        <div class="sep-connect">
                                            <span>Or</span>
                                        </div>
                                        <div class="grp-connect">
                                            <p class="rs">Sign up using your account on:</p>
                                            <a class="btn-flat fb" href="#">Facebook</a>
                                            <a class="btn-flat gg" href="#">Google</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <p class="rs wrap-link-back">
                                <a id="sys_link_login_panel" href="login.html" class="link-back">
                                    <i class="pick-r"></i>
                                    Back to login
                                </a>
                            </p>
                        </div><!--end: Register panel -->
                    </div>
                    <div id="sys_paging_state" class="paging-state">
                        <i class="active"></i>
                        <i></i>
                    </div>
                    <i id="sys_close_login_popup" class="icon iClose close-popop"></i>
                </div>
            </div>
        </header><!--end: header.mod-header -->
        <nav id="mp-menu" class="mp-menu alternate-menu">
            <div class="mp-level">
                <h2>Coupons Junction</h2>
                <ul>
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li class="has-sub">
                        <a href="coupon-code.html">Stores</a>
                        <div class="mp-level">
                            <h2>List of Stores</h2>
                            <a class="mp-back" href="#">back</a>
                            @if($stores)
                            <ul>
                                @foreach($stores as $store)
                                <li><a href="{{url('/').'/'.$store['store_slug']}}">{{$store['store_name']}}</a></li>
                                @endforeach
                            </ul>
                            @endif
                        </div>
                    </li>
                    <li class="has-sub">
                        <a href="brand-list.html">Categories</a>
                        <div class="mp-level">
                            <h2>list of Categories</h2>
                            <a class="mp-back" href="#">back</a>
                            @if($categories)
                            <ul>
                                @foreach($categories as $category)
                                <li><a href="{{url('/').'/category/'.$category['category_slug']}}">{{$category['category_name']}}</a></li>
                                @endforeach
                            </ul>
                            @endif
                        </div>
                    </li>
                    <li><a href="{{url('/')}}">Coupons</a></li>
                    <!-- <li><a href="blog.html">Blog</a></li>
                    <li><a href="my-coupon.html">My coupons(12)</a></li> -->
                    <!-- <li><a href="login.html">Login</a></li> -->
                </ul>
            </div>
        </nav><!--end: .mp-menu -->
