<!DOCTYPE html>
<html>

<head>
    <title>{{MetaTag::get('title')}}</title>
    <meta charset="utf-8">
    {!! MetaTag::tag('title') !!}
    {!! MetaTag::tag('description') !!}

    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:locale" content="en_US">
    <meta property="og:site_name" content="CouponsJunction">
    <meta property="og:title" content="{{MetaTag::get('title')}}">
    <meta property="og:description" content="{{MetaTag::get('description')}}">
    <meta property="og:type" content="Website">

    <meta name="twitter:title" content="{{MetaTag::get('title')}}" />
    <meta name="twitter:description" content="{{MetaTag::get('description')}}" />
    
    <link rel="stylesheet" href="{{asset('public/css/font.css')}}"/>
    <link rel="stylesheet" href="{{asset('public/css/font-awesome.css')}}"/>
    <link rel="stylesheet" href="{{asset('public/css/normalize.css')}}"/>
    <!--css plugin-->
    <link rel="stylesheet" href="{{asset('public/css/flexslider.css')}}"/>
    <link rel="stylesheet" href="{{asset('public/css/jquery.nouislider.css')}}"/>
    <link rel="stylesheet" href="{{asset('public/css/jquery.popupcommon.css')}}"/>

    <link rel="stylesheet" href="{{asset('public/css/style.css')}}"/>
    <link rel="stylesheet" href="{{asset('public/css/style-dark.css')}}">
    <link rel="stylesheet" href="{{asset('public/css/style-gray.css')}}">
    <!--[if IE 9]>
    <link rel="stylesheet" href="css/ie9.css"/>
    <![endif]-->
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="css/ie8.css"/>
    <![endif]-->

    <link rel="stylesheet" href="{{asset('public/css/res-menu.css')}}"/>
    <link rel="stylesheet" href="{{asset('public/css/responsive.css')}}"/>

    <link rel="stylesheet" href="{{asset('public/css/owl.carousel.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('public/css/custom.css')}}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158397522-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-158397522-1');
    </script>

    <script data-ad-client="ca-pub-8332117649672053" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>


    <!--[if lte IE 8]>
        <script type="text/javascript" src="js/html5.js"></script>
    <![endif]-->

</head>