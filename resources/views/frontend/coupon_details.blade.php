@include('frontend.layouts.header')
@include('frontend.layouts.head')
    
    <!-- <div class="top-area">
        <div class="grid_frame">
            <div class="container_grid clearfix">
                <div class="grid_12">
                    <h2 class="page-title">Coupons</h2>
                </div>
            </div>
        </div>
    </div> -->
    <div class="grid_frame page-content">
        <div class="container_grid">
            <div class="mod-breadcrumb clearfix">
                <div class="grid_12">
                    <a href="#">Home</a>
                    <span>></span>
                    <a href="#">Coupons</a>
                    <!-- <span>></span> -->
                    <!-- <a href="#">Lindt - Save 10% off</a> -->
                </div>
            </div><!--end: .mod-breadcrumb -->
            <div class="mod-coupon-detail clearfix">
                <div class="grid_4">
                    <div class="wrap-thumb">
                        <div class="img-thumb-center">
                            <div class="wrap-img-thumb">
                                <span class="ver_hold"></span>
                                <a href="#" class="ver_container"><img src="{{$offer_details->store_logo}}" alt="{{$offer_details->store_logo}}"></a>
                            </div>
                        </div>
                        <!-- <i class="stick-lbl hot-sale"></i> -->
                    </div>
                </div>
                <div class="grid_5">
                    <!-- <div class="save-price">Save 10% Off</div> -->
                    <a href="#" class="brand-name">{{$offer_details->offer_name}}</a>
                    <div class="coupon-desc details-coupon-desc">{!!$offer_details->offer_description!!}</div>
                    <div class="wrap-btn clearfix">
                        <!-- <div class="day-left">9 days 4 hours left</div> -->
                        @if($offer_details->offer_type == 1)
                            <a class="btn btn-blue btn-take-coupon" href="{{$offer_details->offer_url}}" target="_blank">{{$offer_details->coupon_code}}</a>
                        @else
                            <a class="btn btn-blue btn-take-coupon" href="{{$offer_details->offer_url}}" target="_blank">Deal Activated</a>
                        @endif
                    </div>
                    <!-- <div class="wrap-action clearfix">
                        <div class="left-vote">
                            <span class="lbl-work">100% work</span>
                            <span class="lbl-vote">12 <i class="icon iAddVote"></i></span>
                            <span class="lbl-vote">2 <i class="icon iSubVote"></i></span>
                        </div>
                        <div class="right-social">
                            Share now
                            <a href="#"><i class="fa fa-facebook-square fa-2x"></i></a>
                            <a href="#"><i class="fa fa-twitter-square fa-2x"></i></a>
                            <a href="#"><i class="fa fa-pinterest-square fa-2x"></i></a>
                            <a href="#"><i class="fa fa-linkedin-square fa-2x"></i></a>
                        </div>
                    </div> -->
                    <!-- <div class="wrap-tag">
                        <span class="btn btn-gray type-tag tag-lbl">Tag</span>
                        <a class="btn btn-gray type-tag" href="#">Sweet</a>
                        <a class="btn btn-gray type-tag" href="#">Lindor</a>
                        <a class="btn btn-gray type-tag" href="#">Food</a>
                        <a class="btn btn-gray type-tag" href="#">Lindt</a>
                        <a class="btn btn-gray type-tag" href="#">Walmart</a>
                        <a class="btn btn-gray type-tag" href="#">Chocolate</a>
                    </div> -->
                </div>
                <!-- <div class="grid_3">
                    <div class="brand-info ta-c">
                        <div class="brand-logo"><img src="{{$offer_details->store_logo}}" alt=""/></div>
                        <span class="star-rate"><span style="width: 91%"></span></span>
                        <div class="rated-number"></div>
                        <div class="brand-desc ta-l">{{$offer_details->store_description}} </div>
                        <a class="link-brand" href="#">View Brand</a>
                    </div>
                </div> -->
            </div><!--end: .mod-coupon-detail -->
            <div class="mod-grp-coupon block clearfix">
                <div class="grid_12">
                    <h3 class="title-block">
                        Related coupons
                    </h3>
                </div>
                <div class="block-content list-coupon clearfix">
                    @foreach($related_offer as $related)
                    <div class="coupon-item grid_3">
                        <div class="coupon-content popular_offers_content">
                            <div class="img-thumb-center">
                                <div class="wrap-img-thumb">
                                    <span class="ver_hold"></span>
                                    <a href="#" class="ver_container"><img src="{{$related->store_logo}}" alt="{{$related->offer_name}}"></a>
                                </div>
                            </div>
                            <!-- <div class="coupon-price">$2.00 Off</div> -->
                            <div class="coupon-price">{{$related->offer_name}}</div>
                            <div class="coupon-desc"></div>
                            <!-- <div class="time-left">9 days 4 hours left</div> -->
                            <a class="btn btn-blue btn-take-coupon" href="#">Take Coupon</a>
                        </div>
                        <!-- <i class="stick-lbl hot-sale"></i> -->
                    </div><!--end: .coupon-item -->
                    @endforeach
                </div>
             </div><!--end block: Related coupons-->
            <div class="mod-email-newsletter clearfix">
                <div class="grid_12">
                    <div class="wrap-form clearfix">
                        <div class="left-lbl">
                            <div class="big-lbl">newsletter</div>
                            <div class="sml-lbl">Don't miss a chance!</div>
                        </div>
                        <div class="wrap-email">
                            <label for="sys_email_newsletter">
                                <input type="email" id="sys_email_newsletter" placeholder="Enter your email here"/>
                            </label>
                        </div>
                        <button class="btn btn-orange btn-submit-email" type="submit">SUBSCRIBE NOW</button>
                    </div>
                </div>
            </div><!--end: .mod-email-newsletter-->
            <!-- <div class="mod-brands block clearfix">
                <div class="grid_12">
                    <h3 class="title-block has-link">
                        POPULAR BRANDS (129)
                        <a href="#" class="link-right">See all <i class="pick-right"></i></a>
                    </h3>
                </div>
                <div class="block-content list-brand clearfix">
                    <div class="brand-item grid_4">
                        <div class="brand-content">
                            <div class="brand-logo">
                                <div class="wrap-img-logo">
                                    <span class="ver_hold"></span>
                                    <a href="#" class="ver_container"><img src="images/ex/01_07.jpg" alt="$BRAND_TITLE"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> --><!--end: .mod-brand -->
        </div>
    </div>
        
@include('frontend.layouts.footer')